<?php

Route::get('/', ['as' => 'card.index', 'uses' => 'CardController@index']);
Route::get('/{query?}', ['as' => 'card.query', 'uses' => 'CardController@index']);

Route::resource('card', 'CardController', [
    'except' => ['show'],
    'names' => ['create' => 'card.create', 'store' => 'card.store', 'destroy' => 'card.destroy', 'edit' => 'card.edit', 'update' => 'card.update']
]);
