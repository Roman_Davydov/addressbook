<div class="form-group @if($errors->get($form_field_name)) has-error @endif">

	{{ Form::label($field_name, null, ['class' => 'control-label']) }}
	{{ Form::text($form_field_name, old($form_field_name, ($card->$form_field_name ?? null)), ['class' => 'form-control']) }}

	@foreach ($errors->get($form_field_name) as $message)
		{!! $message !!}
	@endforeach
</div>