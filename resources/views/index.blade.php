@extends('master')


@section('content')
	<form class="form-inline" method="get" action="{{route('card.query')}}">
		<div class="form-group">
			<input type="text" class="form-control" name="query" placeholder="search for..." @if(!empty($query))value="{{$query}}"@endif>
		</div>
		<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search</button>
	</form>

	@if(count($list) > 0)
		<table class="table table-striped table-hover table-condensed">
			<thead>
			<tr>
				<th>First name</th>
				<th>Second name</th>
				<th>Position</th>
				<th>Company</th>
				<th>Email</th>
				<th>Email work</th>
				<th>Phone</th>
				<th>Phone work</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			</thead>
			<tbody>
			@foreach($list as $card)
				<tr>
					<td>{{$card->first_name}}</td>
					<td>{{$card->second_name}}</td>
					<td>{{$card->position}}</td>
					<td>{{$card->company}}</td>
					<td>{{$card->email}}</td>
					<td>{{$card->email_work}}</td>
					<td>{{$card->phone}}</td>
					<td>{{$card->phone_work}}</td>
					<td>
						<button>
							<a href="{{route('card.edit', $card->id)}}" title="Edit">
								<span class="glyphicon glyphicon-pencil"></span>
							</a>
						</button>
					</td>
					<td>
						{{ Form::open(['route' => ['card.destroy', $card->id]]) }}
						{{ Form::hidden('_method', 'DELETE') }}
						{{ Form::submit('Delete') }}
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@endif
@endsection
