<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>The LaraAddressBook</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<div class="header">
		<h1>The LaraAddressBook</h1>
	</div>
	<ul class="nav nav-pills">
		<li><a href="{{route('card.index')}}"><span class="glyphicon glyphicon-list"></span> Cards list</a></li>
		<li><a href="{{route('card.create')}}"><span class="glyphicon glyphicon-plus"></span> Add Card</a></li>
	</ul>
	<div class="main">
		@yield('content')
	</div>
</div>
</body>
</html>
