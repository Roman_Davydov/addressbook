@extends('master')


@section('content')
	@if(isset($card))
		<h2>Update card</h2>
	@else
		<h2>Add new card</h2>
	@endif
	<div class="bd-example">
		@if(isset($card))
			{{ Form::open(['route' => ['card.update', $card->id]]) }}
			{{ Form::hidden('_method', 'PATCH') }}
		@else
			{{ Form::open(['route' => 'card.store']) }}
		@endif

		@include('field', ['field_name' => 'First name', 'form_field_name' => 'first_name'])
		@include('field', ['field_name' => 'Second name',  'form_field_name' => 'second_name'])
		@include('field', ['field_name' => 'Position',  'form_field_name' => 'position'])
		@include('field', ['field_name' => 'Company',  'form_field_name' => 'company'])
		@include('field', ['field_name' => 'Email',  'form_field_name' => 'email'])
		@include('field', ['field_name' => 'Email Work',  'form_field_name' => 'email_work'])
		@include('field', ['field_name' => 'Phone',  'form_field_name' => 'phone'])
		@include('field', ['field_name' => 'Phone Work',  'form_field_name' => 'phone_work'])

		{{ Form::submit('Submit') }}
		{{ Form::close() }}
	</div>
@endsection
