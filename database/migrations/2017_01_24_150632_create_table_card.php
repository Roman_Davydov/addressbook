<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCard extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('card', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('second_name');
			$table->string('company')->nullable();
			$table->string('position')->nullable();
			$table->string('email')->nullable();
			$table->string('email_work')->nullable();
			$table->string('phone')->nullable();
			$table->string('phone_work')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('card');
	}
}