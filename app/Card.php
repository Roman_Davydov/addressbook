<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'card';

	protected $fillable = ['first_name', 'second_name', 'position', 'company', 'email', 'email_work', 'phone', 'phone_work'];
}