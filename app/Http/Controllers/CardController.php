<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCardRequest;
use Illuminate\Http\Request;
use App\Card;

class CardController extends Controller
{
    public function index(Request $request)
    {
        $list = Card::all();
        if ($request->has('query')) {
            $query = strtolower($request->get('query'));
            $data['query'] = $query;
            $list = $list->filter(function ($item) use ($query) {
                return (is_numeric(strpos(strtolower($item->first_name), $query)) || is_numeric(strpos(strtolower($item->second_name), $query)));
            });
        }
        $data['list'] = $list->sortBy('id');
        return view('index', $data);
    }

    public function create()
    {
        return view('form');
    }

    public function edit(Card $card)
    {
        return view('form', compact('card'));
    }

    public function destroy(Card $card)
    {
        $card->delete();
        return redirect()->route('card.index');
    }

    public function update(StoreCardRequest $request, Card $card)
    {
        $card->update($request->all());
        return redirect()->route('card.index');
    }

    public function store(StoreCardRequest $request)
    {
        Card::create($request->all());
        return redirect()->route('card.index');
    }
}
